__author__ = "Mohamed Yehia El-Morsy"
__credits__ = ["Mohamed Yehia"]
__version__ = "1.0"
__email__ = "M.yehia@artronix.net"
__status__ = "Developing"

from PyQt4 import QtGui ,QtCore
import MyoConnector
import sys
import os
import pyqtgraph as pg
import numpy as np
import csv
from styles.qsshelper import QSSHelper
from userInterface.mainInterface import Ui_Form
class MyoInterfaceCaller(QtGui.QWidget , Ui_Form):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.updatePlotTime = QtCore.QTimer()
        self.updatePlotTime.timeout.connect(self.updatePlots)
        self.plotFigures = []
        self.signalArrays = []
        self.addPlots()
        self.myo = MyoConnector.QMyoConnector()
        self.myo.newSignalRecieved.newReadComes.connect(self.recieve_new_signal)
        self.myo.newReadString.newReadComes.connect(self.recieve_new_event)
        self.myo.start()
        self.currentPregress = 0
        self.currentMode = "Running"
        self.PATHTODATASET = os.path.join(os.getcwd() , "registered_gesture")
        self.enableRecording = False
        self.waitingNextEvent = False
        self.shiftsCount = 0
        self.featureBuffer = []
        self.currentTrainingCounts = 0
        self.onsetDetected = False
        #signals
        self.RunningModes.setCurrentIndex(0)
        self.RunningModes.currentChanged.connect(self.windowChanged)
        self.record_btn.clicked.connect(self.startRecording)

        self.new_gesture_opt.toggled.connect(lambda: self.radioBtnToggled(self.new_gesture_opt))
        self.retrain_gesture_opt.toggled.connect(lambda: self.radioBtnToggled(self.retrain_gesture_opt))
        self.remove_gesture_opt.toggled.connect(lambda: self.radioBtnToggled(self.remove_gesture_opt))
        qss = QSSHelper.open_qss(os.path.join('styles', 'aqua.qss'))
        #self.setStyleSheet(qss)
        self.show()

    @QtCore.pyqtSlot(str)
    def recieve_new_event(self ,evn):
        self.current_gesture_lbl.setText(evn)

    def radioBtnToggled(self , btn):
        if self.new_gesture_opt.isChecked():
            self.setTrainingMode()
        elif self.retrain_gesture_opt.isChecked():
            self.setRetrainingMode()
        elif self.remove_gesture_opt.isChecked():
            self.setDeleteMode()

    def startRecording(self):
        print self.currentMode
        if self.currentMode == "Training":
            tempName = str(self.gesture_neme_edit.text()).strip()
            if len(tempName):
                self.file_path = os.path.join(self.PATHTODATASET , tempName+".csv" )
                if not os.path.isfile(self.file_path):
                    self.disableAll()
                    self.enableRecording = True
                    self.waitingNextEvent = False
                    self.currentTrainingCounts = 0
                    self.featureBuffer = []
                else:
                    print("file already exist")
            else:
                print("write guster name first")



    def disableAll(self):
        self.new_gesture_opt.setEnabled(False)
        self.retrain_gesture_opt.setEnabled(False)
        self.remove_gesture_opt.setEnabled(False)

        self.gesture_neme_edit.setEnabled(False)
        self.number_of_records_choose.setEnabled(False)
        self.avilable_gesture.setEnabled(False)
        self.train_btn.setEnabled(False)
        self.clear_btn.setEnabled(False)




    def setDeleteMode(self):
        self.currentPregress = 0
        self.currentMode = "Deleting"
        self.recording_progress.setValue(0)
        self.gesture_neme_edit.setEnabled(False)
        self.avilable_gesture.setEnabled(True)
        self.record_btn.setEnabled(False)
        self.stop_btn.setEnabled(False)
        self.train_btn.setEnabled(False)
        self.clear_btn.setEnabled(True)
        self.number_of_records_choose.setEnabled(False)
    def setTrainingMode(self):
        self.currentPregress = 0
        self.currentMode = "Training"
        self.recording_progress.setValue(0)
        self.gesture_neme_edit.setEnabled(True)
        self.avilable_gesture.setEnabled(True)
        self.record_btn.setEnabled(True)
        self.stop_btn.setEnabled(True)
        self.train_btn.setEnabled(True)
        self.clear_btn.setEnabled(True)
        self.number_of_records_choose.setEnabled(True)
    def setRetrainingMode(self):
        self.currentPregress = 0
        self.currentMode = "Retraining"
        self.recording_progress.setValue(0)
        self.gesture_neme_edit.setEnabled(False)
        self.avilable_gesture.setEnabled(True)
        self.record_btn.setEnabled(True)
        self.stop_btn.setEnabled(True)
        self.train_btn.setEnabled(True)
        self.clear_btn.setEnabled(True)
        self.number_of_records_choose.setEnabled(True)

    def windowChanged(self):
        tabIndex = self.RunningModes.currentIndex()
        if tabIndex == 1:
            self.new_gesture_opt.setChecked(True)
            self.setTrainingMode()

    def addPlots(self):
        plotLayouts = [self.gridLayout_1 , self.gridLayout_2 , self.gridLayout_3 ,self.gridLayout_4,
                       self.gridLayout_5, self.gridLayout_6, self.gridLayout_7, self.gridLayout_8,]
        for i in range(8):
            temp = pg.PlotWidget()
            temp.setStyleSheet("border:none;")
            temp.setBackgroundBrush(QtGui.QBrush(QtGui.QColor(230,230,230)))
            temp.getAxis('bottom').setPen(70, 110, 130)
            temp.getAxis('left').setPen(70, 110, 130)
            temp.setYRange(0,2000)
            plotLayouts[i].addWidget(temp)
            self.plotFigures.append(temp)
            self.signalArrays.append(np.zeros(1000))
        self.updatePlotTime.start(100)

    @QtCore.pyqtSlot(tuple)
    def recieve_new_signal(self , emgPacket):
        if len(emgPacket) == 8 :
            for i in range(8):
                self.signalArrays[i] = np.roll(self.signalArrays[i] ,-1)
                self.signalArrays[i][-1] = emgPacket[i]
                if np.average(self.signalArrays[i][950:])>300 and self.onsetDetected == False:
                    self.onsetDetected = True
                    self.shiftsCount = 0

                self.shiftsCount += 1


            if self.onsetDetected == True and self.shiftsCount>100:
                if self.enableRecording == True and not self.waitingNextEvent:
                    currentFeature = []
                    print "new record taken"
                    for i in range(8):
                        currentFeature.append(np.average(self.signalArrays[i][800:]))
                    self.shiftsCount = 0
                    self.featureBuffer.append(currentFeature)
                    self.waitingNextEvent = True
                    self.currentTrainingCounts +=1
                    if self.currentTrainingCounts >= 10:
                        self.enableRecording = False
                        fObj = open(self.file_path ,"w")
                        csvWriter = csv.writer(fObj)
                        for row in self.featureBuffer:
                            csvWriter.writerow(row )
                        fObj.close()

                    else:
                       QtCore.QTimer.singleShot(2000, self.takeNextSample)
                self.onsetDetected = False


    def takeNextSample(self):
        self.onsetDetected = False
        self.waitingNextEvent = False
        print "make gusture again"

    def updatePlots(self):
        for i in range(8):
            self.plotFigures[i].clear()
            self.plotFigures[i].plot(self.signalArrays[i] , pen=(70,110,130))

def Main():
    app = QtGui.QApplication(sys.argv)
    windowObject = MyoInterfaceCaller()
    app.exec_()
if __name__ =="__main__":
    Main()
