import myo
from PyQt4 import QtCore
from utilities import *
from enum import Enum

class PoseType(Enum):
	REST = 0
	FIST = 1
	WAVE_IN = 2
	WAVE_OUT = 3
	FINGERS_SPREAD = 4
	DOUBLE_TAP = 5
	UNKNOWN = 255
class Communicate(QtCore.QObject):
    newReadComes = QtCore.pyqtSignal([tuple])
class CommunicateString(QtCore.QObject):
    newReadComes = QtCore.pyqtSignal([str])

class QMyoConnector( QtCore.QThread):
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.newSignalRecieved = Communicate()
        self.newReadString = CommunicateString()
    def run(self):
        self.myoObj = myo.Myo()
        self.myoObj.connect()
        self.myoObj.add_listener(self.handle_data)
        while True :
            self.myoObj.run()


    def handle_data(self, data):
        if data.cls != 4 and data.command != 5:
            return
        connection, attribute, data_type = unpack('BHB', data.payload[:4])
        payload = data.payload[5:]
        if attribute == 39:
            temp = unpack('8HB', payload)
            emgSignal = temp[:8]
            self.newSignalRecieved.newReadComes.emit(emgSignal)
        elif attribute == 0x23:
            data_type, value, address, x, y, z = unpack('6B', payload)
            if data_type == 3:
                #self.on_pose(value)
                pose_type = PoseType(value)
                self.newReadString.newReadComes.emit(str(pose_type))
